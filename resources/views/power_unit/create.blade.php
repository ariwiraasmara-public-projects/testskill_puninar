@extends('index')
@section('content')
<div class="p-30 is-mobile text-is-black">
    <h1 class="title center text-is-black">Create Power Unit</h1>

    @if(Session::has('msg'))
        <div id="notif" class="notification is-danger m-t-10 m-b-10">
            <button id="close-notif" class="delete"></button>
            {{ Session::get('msg'); }}
        </div>
    @endif

    <form action="{{ route('powerunit_save') }}" method="post">
        {{ csrf_field() }}
        {{ method_field('POST') }}
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <div class="control">
            <input class="input" type="text" name="powerunit" placeholder="Number.." />
        </div>

        <div class="control m-t-10">
            <textarea class="textarea" name="description" placeholder="Description.."></textarea>
        </div>

        <div class="columns m-t-10 is-mobile">
            <div class="column">
                <div class="control is-6">
                    <div class="select is-fullwidth">
                        <select name="corporation">
                            <option value="" disabled selected>-- Choose Corporation --</option>
                            @foreach($corporation as $cp)
                                <option value="{{ $cp['ID_Corporation'] }}">{{ $cp['Corporation_Name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="column">
                <div class="control is-6">
                    <div class="select is-fullwidth">
                        <select name="location">
                            <option value="" disabled selected>-- Choose Location --</option>
                            @foreach($location as $lc)
                                <option value="{{ $lc['ID_Location'] }}">{{ $lc['Location_Name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="columns m-t-10 is-mobile">
            <div class="column">
                <div class="control is-6">
                    <div class="select is-fullwidth">
                        <select name="type">
                            <option value="" disabled selected>-- Choose Power Unit Type --</option>
                            @foreach($power_unit_type as $put)
                                <option value="{{ $put['ID_Power_Unit_Type'] }}">{{ $put['Power_Unit_Type_XID'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="column">
                <div class="control is-6">
                    <span>Is Active?</span><br/>
                    <label class="radio">
                        <input type="radio" name="isactive" value="Y">
                        Yes
                    </label>
                    
                    <label class="radio">
                        <input type="radio" name="isactive" value="N">
                        No
                    </label>
                </div>
            </div>
        </div>

        <div class="buttons is-centered has-addons is-fullwidth">
            <a href="/powerunit" class="button is-link is-rounded"><ion-icon name="arrow-back-outline"></ion-icon></a>
            <button type="submit" class="button is-success is-rounded">Save</button>
        </div>

    </form>
</div>

<script>
    document.title = "Create Power Unit | Test Skill Puninar Logistik";
</script>
@endsection