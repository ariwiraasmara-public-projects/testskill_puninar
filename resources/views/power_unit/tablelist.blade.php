@extends('index')
@section('content')
<div class="p-30">
    <h1 class="title center txt-black">Power Unit</h1>

    <div class="m-t-30 m-b-30 center">
        <a href="{{ route('powerunit_create') }}" class="button is-link is-rounded">Add</a>
        @if(Session::has('msg'))
            <div id="notif" class="notification is-success m-t-10 m-b-10">
                <button id="close-notif" class="delete"></button>
                {{ Session::get('msg'); }}
            </div>
        @endif
    </div>

    <table class="table is-striped is-mobile is-full">
        <thead>
            <tr>
                <th class="center">ID Power Unit</th>
                <th class="">Power Unit</th>
                <th class="">Description</th>
                <th class="">ID Corporation</th>
                <th class="">ID Location</th>
                <th class="">ID Power Unit Type</th>
                <th class="">Is Active?</th>
                <th colspan="2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach($data as $d)
                <tr>
                    <td class="right">{{ $d['ID_Power_Unit'] }}</td>
                    <td>{{ $d['Power_Unit_Num'] }}</td>
                    <td>{{ $d['Description'] }}</td>
                    <td>{{ $d['Corporation_Name'] }}</td>
                    <td>{{ $d['Location_Name'] }}</td>
                    <td>{{ $d['Power_Unit_Type_XID'] }}</td>
                    <td class="center">{{ ($d['Is_Active'] == 'Y') ? 'Yes' : 'No'  }}</td>
                    <td><a href="/powerunit/edit/{{ $d['ID_Power_Unit'] }}" class="button is-link">Edit</a></td>
                    <td><button class="button is-danger" id="{{ 'deleteid'.$d['ID_Power_Unit'] }}" value="{{ $d['ID_Power_Unit'] }}" onClick="toDelete({{ $d['ID_Power_Unit'] }})" >Delete</button></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<script>
    document.title = "Power Unit | Test Skill Puninar Logistik";

    function toDelete(idt) {
        let token   = $("meta[name='csrf-token']").attr("content");
        let id      = $('#deleteid' + idt).val();

        $.ajax({
            url: '/powerunit/delete/' + id,
            type : 'POST',
            data: {
                "_token": token,
			    id: id
			},  
			success: function(data) {
                alert('Data Berhasil Dihapus! Silahkan reload!');
			},
			error : function(error) {
			    alert('Telah terjadi error!\nTidak bisa delete data!');
                console.log(error);
			    return false;
			}
		});
    }

    $('#close-notif').click(function(){
        $('#notif').addClass('is-hidden');
    });
</script>
@endsection