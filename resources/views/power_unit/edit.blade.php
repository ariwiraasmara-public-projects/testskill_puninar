@extends('index')
@section('content')
<div class="p-30 is-mobile text-is-black">
    <h1 class="title center text-is-black">Edit Power Unit</h1>

    @if(Session::has('msg'))
        <div id="notif" class="notification is-danger m-t-10 m-b-10">
            <button id="close-notif" class="delete"></button>
            {{ Session::get('msg'); }}
        </div>
    @endif

    <form action="{{ '/powerunit/update/'.$data[0]['ID_Power_Unit'] }}" method="post">
        {{ csrf_field() }}
        {{ method_field('POST') }}
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <input type="number" name="id" value="{{ $data[0]['ID_Power_Unit'] }}" class="is-hidden" readonly />

        <div class="control">
            <input class="input" type="text" name="powerunit" placeholder="Number" value="{{ $data[0]['Power_Unit_Num'] }}" />
        </div>

        <div class="control m-t-10">
            <textarea class="textarea" name="description" placeholder="Description">{{ $data[0]['Description'] }}</textarea>
        </div>

        <div class="columns m-t-10 is-mobile">
            <div class="column">
                <div class="control m-t-0">
                    <div class="select">
                        <select name="corporation">
                            <option value="" disabled selected>-- Choose Corporation --</option>
                            @foreach($corporation as $cp)
                                <option value="{{ $cp['ID_Corporation'] }}" 
                                        @if($cp['ID_Corporation'] == $data[0]['ID_Corporation']) 
                                            selected
                                        @endif >{{ $cp['Corporation_Name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="column">
                <div class="control m-t-0">
                    <div class="select">
                        <select name="location">
                            <option value="" disabled selected>-- Choose Location --</option>
                            @foreach($location as $lc)
                                <option value="{{ $lc['ID_Location'] }}"
                                    @if($lc['ID_Location'] == $data[0]['ID_Location'])
                                        selected
                                    @endif >{{ $lc['Location_Name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="columns m-t-10 is-mobile">
            <div class="column">
                <div class="control m-t-0">
                    <div class="select">
                        <select name="type">
                            <option value="" disabled selected>-- Choose Power Unit Type --</option>
                            @foreach($power_unit_type as $put)
                                <option value="{{ $put['ID_Power_Unit_Type'] }}" 
                                    @if($put['ID_Power_Unit_Type'] == $data[0]['ID_Power_Unit_Type'])
                                        selected
                                    @endif>{{ $put['Power_Unit_Type_XID'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="column">
                <div class="control">
                    <span>Is Active?</span><br/>
                    <label class="radio">
                        <input type="radio" name="isactive" value="Y" @if($data[0]['Is_Active'] == 'Y') checked @endif >
                        Yes
                    </label>
                    
                    <label class="radio">
                        <input type="radio" name="isactive" value="N" @if($data[0]['Is_Active'] == 'N') checked @endif >
                        No
                    </label>
                </div>
            </div>
        </div>

        <div class="buttons is-centered has-addons is-fullwidth">
            <a href="/powerunit" class="button is-link is-rounded"><ion-icon name="arrow-back-outline"></ion-icon></a>
            <button type="submit" class="button is-success is-rounded">Save</button>
        </div>

    </form>
</div>

<script>
    document.title = "Edit Power Unit | Test Skill Puninar Logistik";
</script>
@endsection