@extends('index')
@section('content')
<div class="p-30">
    <h1 class="title center txt-black">Maps</h1>

    <div class="columns is-mobile">
        <div class="column is-6">
            <iframe src="https://maps.google.com/maps?q=Tangesir%20Dates%20Products&amp;t=&amp;z=13&amp;ie=UTF8&amp;iwloc=&amp;output=embed" 
                width=100% height=500 allowfullscreen></iframe>
        </div>

        <div class="column is-6">
            <div class="select is-fullwidth">
                <select name="location" id="location">
                    <option value="" disabled selected>-- Choose Location --</option>
                    @foreach($data as $loc)
                        <option value="{{ $loc['ID_Location'] }}">{{ $loc['Location_Name'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    
</div>

<script>
    document.title = "Maps | Skill Test Puninar Logistik"
</script>
@endsection