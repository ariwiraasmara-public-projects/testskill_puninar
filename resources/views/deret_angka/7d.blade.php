@extends('index')
@section('content')
@php 
$size = 5;
$space = $size - 1;
$num = 1;

for($i = 1; $i <= $size; $i++) {
    for($j = 1; $j <= $size; $j++) {
        echo "&nbsp;";
    }

    for($j = 1; $j <= $num; $j++) {
        echo $j;
    }

    for($j = $num - 1; $j >= 1; $j--) {
        echo $j;
    }
    echo "<br/>";
    $num++;
    //$space++;
}

$num -= 2;
$space = 1;

for($i = $size; $i >= 1; $i--) {
    for($j = 1; $j <= $space; $j++) {
        echo "&nbsp;&nbsp;";
    }

    for($j = 1; $j <= $num; $j++) {
        echo $j;
    }

    for($j = $num - 1; $j >= 1; $j--) {
        echo $j;
    }
    echo "<br/>";
    $num--;
    $space++;
}
@endphp
@endsection