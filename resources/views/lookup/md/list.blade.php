@extends('index')
@section('content')
<div class="p-30">
    <h1 class="title center txt-black">Lookup MD</h1>

    <div class="m-t-30 m-b-30 center">
        <a href="{{ route('md_create') }}" class="button is-link is-rounded">Add</a>
        @if(Session::has('msg'))
            <div id="notif" class="notification is-success m-t-10 m-b-10">
                <button id="close-notif" class="delete"></button>
                {{ Session::get('msg'); }}
            </div>
        @endif
    </div>

    <table class="table is-fullwidth is-mobile">
        <thead>
            <tr>
                <th class="center">Lookup Lines ID</th>
                <th class="center">Lookup ID</th>
                <th>Lookup Lines Code</th>
                <th>Description</th>
                <th>Effective From</th>
                <th>Effective To</th>
                <th colspan="2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach($data as $md)
                <tr>
                    <td class="right">{{ $md['lookup_lines_id'] }}</td>
                    <td class="right">{{ $md['lookup_code'] }}</td>
                    <td>{{ $md['lookup_lines_code'] }}</td>
                    <td>{{ $md['description'] }}</td>
                    <td>{{ $md['effective_from'] }}</td>
                    <td>{{ $md['effective_to'] }}</td>
                    <td><a href="/lookup/md/edit/{{ $md['lookup_lines_id'] }}" class="button is-link">Edit</a></td>
                    <td><button class="button is-danger" id="{{ 'deleteid'.$md['lookup_lines_id'] }}" value="{{ $md['lookup_lines_id'] }}" onClick="toDelete({{ $md['lookup_lines_id'] }})" >Delete</button></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<script>
    document.title = "Lookup MD | Test Skill Puninar Logistik";

    function toDelete(idt) {
        let token   = $("meta[name='csrf-token']").attr("content");
        let id      = $('#deleteid' + idt).val();

        $.ajax({
            url: '/lookup/md/delete/' + id,
            type : 'POST',
            data: {
                "_token": token,
			    id: id
			},  
			success: function(data) {
                alert('Data Berhasil Dihapus! Silahkan reload!');
			},
			error : function(error) {
			    alert('Telah terjadi error!\nTidak bisa delete data!');
                console.log(error);
			    return false;
			}
		});
    }

    $('#close-notif').click(function(){
        $('#notif').addClass('is-hidden');
    });
</script>
@endsection