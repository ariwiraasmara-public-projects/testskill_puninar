@extends('index')
@section('content')
<div class="p-30">
    <h1 class="title center text-is-black">Edit Lookup MD</h1>

    @if(Session::has('msg'))
        <div id="notif" class="notification is-danger m-t-10 m-b-10">
            <button id="close-notif" class="delete"></button>
            {{ Session::get('msg'); }}
        </div>
    @endif

    <form action="{{ '/lookup/md/update/'.$data[0]['lookup_lines_id'] }}" method="post">
        {{ csrf_field() }}
        {{ method_field('POST') }}
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <input type="text" name="id" class="is-hidden" value="{{ $data[0]['lookup_lines_id'] }} " />
        
        <div class="columns">
            <div class="column is-6">
                <div class="control">
                    <div class="select is-fullwidth">
                        <select name="lookup_id">
                            <option value="" disabled selected>-- Choose Lookup MH --</option>
                            @foreach($lookupid as $lid)
                                <option value="{{ $lid['lookup_id'] }}"
                                    @if($lid['lookup_id'] == $data[0]['lookup_code'])
                                        selected
                                    @endif>{{ $lid['lookup_code'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="column is-6">
                <div class="control">
                    <input class="input" type="text" name="lines_code" placeholder="Lookup Lines Code.." value="{{ $data[0]['lookup_lines_code'] }}" />
                </div>
            </div>
        </div>
        
        <div class="control m-t-10">
            <textarea class="textarea" name="description" placeholder="Description..">{{ $data[0]['description'] }}</textarea>
        </div>

        <div class="control m-t-10">
            <div class="columns">
                <div class="column is-6">
                    <label>Effective From: </label><br/>
                    <input type="date" class="input" name="effective_from" value="{{ $data[0]['effective_from'] }}" />
                </div>

                <div class="column is-6">
                    <label>Effective To: </label><br/>
                    <input type="date" class="input" name="effective_to" value="{{ $data[0]['effective_to'] }}" />
                </div>
            </div>
        </div>

        <div class="buttons is-centered has-addons is-fullwidth">
            <a href="/lookup/md/" class="button is-link is-rounded"><ion-icon name="arrow-back-outline"></ion-icon></a>
            <button type="submit" class="button is-success is-rounded">Save</button>
        </div>
    </form>
</div>

<script>
    document.title = "Edit Lookup MD | Test Skill Puninar Logistik";
</script>
@endsection