@extends('index')
@section('content')
<div class="p-30 text-is-black">
    <h1 class="title center text-is-black">Create Lookup MD</h1>

    @if(Session::has('msg'))
        <div id="notif" class="notification is-danger m-t-10 m-b-10">
            <button id="close-notif" class="delete"></button>
            {{ Session::get('msg'); }}
        </div>
    @endif

    <form action="{{ route('md_save') }}" method="post">
        {{ csrf_field() }}
        {{ method_field('POST') }}
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <div class="columns">
            <div class="column is-6">
                <div class="control">
                    <div class="select is-fullwidth">
                        <select name="lookup_id">
                            <option value="" disabled selected>-- Choose Lookup MH --</option>
                            @foreach($lookupid as $lid)
                                <option value="{{ $lid['lookup_id'] }}">{{ $lid['lookup_code'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="column is-6">
                <div class="control">
                    <input class="input" type="text" name="lines_code" placeholder="Lookup Lines Code.." />
                </div>
            </div>
        </div>

        <div class="control m-t-10">
            <textarea class="textarea" name="description" placeholder="Description.."></textarea>
        </div>

        <div class="control m-t-10">
            <div class="columns">
                <div class="column is-6">
                    <label>Effective From: </label><br/>
                    <input type="date" class="input" name="effective_from" />
                </div>

                <div class="column is-6">
                    <label>Effective To: </label><br/>
                    <input type="date" class="input" name="effective_to" />
                </div>
            </div>
        </div>

        <div class="buttons is-centered has-addons is-fullwidth">
            <a href="/lookup/md/" class="button is-link is-rounded"><ion-icon name="arrow-back-outline"></ion-icon></a>
            <button type="submit" class="button is-success is-rounded">Save</button>
        </div>
    </form>
</div>

<script>
    document.title = "Create Lookup MD | Test Skill Puninar Logistik";
</script>
@endsection