@extends('index')
@section('content')
<div class="p-30">
    <h1 class="title center txt-black">Edit Lookup MH</h1>

    @if(Session::has('msg'))
        <div id="notif" class="notification is-danger m-t-10 m-b-10">
            <button id="close-notif" class="delete"></button>
            {{ Session::get('msg'); }}
        </div>
    @endif

    <form action="{{ '/lookup/mh/update/'.$data[0]['lookup_id'] }}" method="post">
        {{ csrf_field() }}
        {{ method_field('POST') }}
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <input class="is-hidden" type="text" name="lookup_code" value="{{ $data[0]['lookup_id'] }}" readonly />
        <div class="control">
            <input class="input" type="text" name="lookup_code" placeholder="Lookup Code.." value="{{ $data[0]['lookup_code'] }}" />
        </div>

        <div class="control m-t-10">
            <textarea class="textarea" name="description" placeholder="Description..">{{ $data[0]['description'] }}</textarea>
        </div>

        <div class="buttons is-centered has-addons is-fullwidth">
            <a href="/lookup/mh/" class="button is-link is-rounded"><ion-icon name="arrow-back-outline"></ion-icon></a>
            <button type="submit" class="button is-success is-rounded">Save</button>
        </div>
    </form>
</div>

<script>
    document.title = "Edit Lookup MH | Test Skill Puninar Logistik";
</script>
@endsection