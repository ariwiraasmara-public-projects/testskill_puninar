@extends('index')
@section('content')
<div class="p-30">
    <h1 class="title center txt-black">Master Lookup MH</h1>

    <div class="m-t-30 m-b-30 center">
        <a href="{{ route('mh_create') }}" class="button is-link is-rounded">Add</a>
        @if(Session::has('msg'))
            <div id="notif" class="notification is-success m-t-10 m-b-10">
                <button id="close-notif" class="delete"></button>
                {{ Session::get('msg'); }}
            </div>
        @endif
    </div>

    <table class="table is-fullwidth is-mobile">
        <thead>
            <tr>
                <th class="center">Lookup ID</th>
                <th class="">Lookup Code</th>
                <th class="">Description</th>
                <th colspan="2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach($data as $mh)
                <tr>
                    <td class="right">{{ $mh['lookup_id'] }}</td>
                    <td><a href="{{ '/lookup/mh/detail/'.$mh['lookup_id'] }}">{{ $mh['lookup_code'] }}</a></td>
                    <td>{{ $mh['description'] }}</td>
                    <td><a href="/lookup/mh/edit/{{ $mh['lookup_id'] }}" class="button is-link">Edit</a></td>
                    <td><button class="button is-danger" id="{{ 'deleteid'.$mh['lookup_id'] }}" value="{{ $mh['lookup_id'] }}" onClick="toDelete({{ $mh['lookup_id'] }})" >Delete</button></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<script>
    document.title = "Lookup MH | Test Skill Puninar Logistik";

    function toDelete(idt) {
        let token   = $("meta[name='csrf-token']").attr("content");
        let id      = $('#deleteid' + idt).val();

        $.ajax({
            url: '/lookup/mh/delete/' + id,
            type : 'POST',
            data: {
                "_token": token,
			    id: id
			},  
			success: function(data) {
                alert('Data Berhasil Dihapus! Silahkan reload!');
			},
			error : function(error) {
			    alert('Telah terjadi error!\nTidak bisa delete data!');
                console.log(error);
			    return false;
			}
		});
    }

    $('#close-notif').click(function(){
        $('#notif').addClass('is-hidden');
    });
</script>
@endsection