@extends('index')
@section('content')
<div class="p-30">
    

    <div class="buttons is-centered has-addons is-fullwidth">
        <a href="/lookup/mh/" class="button is-link is-rounded"><ion-icon name="arrow-back-outline"></ion-icon></a>
        <p class="m-l-10 control"> 
            <h1 class="m-t-10 title text-is-black">Master Lookup MH Detail</h1>
        </p>
    </div>

    <table class="table is-fullwidth is-mobile">
        <thead>
            <tr>
                <th class="center">Lookup Lines ID</th>
                <th class="center">Lookup ID</th>
                <th>Lookup Lines Code</th>
                <th>Description</th>
                <th>Effective From</th>
                <th>Effective To</th>
            </tr>
        </thead>

        <tbody>
            @foreach($data as $md)
                <tr>
                    <td class="right">{{ $md['lookup_lines_id'] }}</td>
                    <td class="right">{{ $md['lookup_code'] }}</td>
                    <td>{{ $md['lookup_lines_code'] }}</td>
                    <td>{{ $md['description'] }}</td>
                    <td>{{ $md['effective_from'] }}</td>
                    <td>{{ $md['effective_to'] }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<script>
    document.title = "Lookup MH Detail | Test Skill Puninar Logistik";
</script>
@endsection