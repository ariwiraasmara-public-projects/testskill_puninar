<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PowerUnitController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\DeretAngka;
use App\Http\Controllers\GnMhLookupController;
use App\Http\Controllers\GnMdLookupController;
// use Redirect;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/', function() {
    return view('home');
});

Route::get('/powerunit', '\App\Http\Controllers\PowerUnitController@index')->name('powerunit_list');
Route::get('/powerunit/create', '\App\Http\Controllers\PowerUnitController@create')->name('powerunit_create');
Route::post('/powerunit/save', '\App\Http\Controllers\PowerUnitController@store')->name('powerunit_save');
Route::get('/powerunit/edit/{id}', '\App\Http\Controllers\PowerUnitController@edit')->name('powerunit_edit');
Route::post('/powerunit/update/{id}', '\App\Http\Controllers\PowerUnitController@update')->name('powerunit_update');
Route::post('/powerunit/delete/{id}', '\App\Http\Controllers\PowerUnitController@destroy')->name('powerunit_delete');

Route::get('/lookup/mh/', '\App\Http\Controllers\GnMhLookupController@index')->name('mh_home');
Route::get('/lookup/mh/create', '\App\Http\Controllers\GnMhLookupController@create')->name('mh_create');
Route::post('/lookup/mh/save', '\App\Http\Controllers\GnMhLookupController@store')->name('mh_save');
Route::get('/lookup/mh/detail/{id}', '\App\Http\Controllers\GnMhLookupController@show')->name('mh_detail');
Route::get('/lookup/mh/edit/{id}', '\App\Http\Controllers\GnMhLookupController@edit')->name('mh_edit');
Route::post('/lookup/mh/update/{id}', '\App\Http\Controllers\GnMhLookupController@update')->name('mh_update');
Route::post('/lookup/mh/delete/{id}', '\App\Http\Controllers\GnMhLookupController@destroy')->name('mh_delete');

Route::get('/lookup/md/', '\App\Http\Controllers\GnMdLookupController@index')->name('md_home');
Route::get('/lookup/md/create', '\App\Http\Controllers\GnMdLookupController@create')->name('md_create');
Route::post('/lookup/md/save', '\App\Http\Controllers\GnMdLookupController@store')->name('md_save');
Route::get('/lookup/md/edit/{id}', '\App\Http\Controllers\GnMdLookupController@edit')->name('md_edit');
Route::post('/lookup/md/update/{id}', '\App\Http\Controllers\GnMdLookupController@update')->name('md_update');
Route::post('/lookup/md/delete/{id}', '\App\Http\Controllers\GnMdLookupController@destroy')->name('md_delete');

Route::get('/maps', 'App\Http\Controllers\LocationController@index')->name('maps');

Route::get('/deretangka/7a', '\App\Http\Controllers\DeretAngka@deretangka7a')->name('deretangka7a');
Route::get('/deretangka/7b', '\App\Http\Controllers\DeretAngka@deretangka7b')->name('deretangka7b');
Route::get('/deretangka/7c', '\App\Http\Controllers\DeretAngka@deretangka7c')->name('deretangka7c');
Route::get('/deretangka/7d', '\App\Http\Controllers\DeretAngka@deretangka7d')->name('deretangka7d');
