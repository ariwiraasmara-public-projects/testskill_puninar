
<?php $__env->startSection('content'); ?>
<div class="p-30 is-mobile text-is-black">
    <h1 class="title center text-is-black">Create Power Unit</h1>

    <?php if(Session::has('msg')): ?>
        <div id="notif" class="notification is-danger m-t-10 m-b-10">
            <button id="close-notif" class="delete"></button>
            <?php echo e(Session::get('msg')); ?>

        </div>
    <?php endif; ?>

    <form action="<?php echo e(route('powerunit_save')); ?>" method="post">
        <?php echo e(csrf_field()); ?>

        <?php echo e(method_field('POST')); ?>

        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <div class="control">
            <input class="input" type="text" name="powerunit" placeholder="Number.." />
        </div>

        <div class="control m-t-10">
            <textarea class="textarea" name="description" placeholder="Description.."></textarea>
        </div>

        <div class="columns m-t-10 is-mobile">
            <div class="column">
                <div class="control is-6">
                    <div class="select is-fullwidth">
                        <select name="corporation">
                            <option value="" disabled selected>-- Choose Corporation --</option>
                            <?php $__currentLoopData = $corporation; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($cp['ID_Corporation']); ?>"><?php echo e($cp['Corporation_Name']); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="column">
                <div class="control is-6">
                    <div class="select is-fullwidth">
                        <select name="location">
                            <option value="" disabled selected>-- Choose Location --</option>
                            <?php $__currentLoopData = $location; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($lc['ID_Location']); ?>"><?php echo e($lc['Location_Name']); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="columns m-t-10 is-mobile">
            <div class="column">
                <div class="control is-6">
                    <div class="select is-fullwidth">
                        <select name="type">
                            <option value="" disabled selected>-- Choose Power Unit Type --</option>
                            <?php $__currentLoopData = $power_unit_type; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $put): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($put['ID_Power_Unit_Type']); ?>"><?php echo e($put['Power_Unit_Type_XID']); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="column">
                <div class="control is-6">
                    <span>Is Active?</span><br/>
                    <label class="radio">
                        <input type="radio" name="isactive" value="Y">
                        Yes
                    </label>
                    
                    <label class="radio">
                        <input type="radio" name="isactive" value="N">
                        No
                    </label>
                </div>
            </div>
        </div>

        <div class="buttons is-centered has-addons is-fullwidth">
            <a href="/powerunit" class="button is-link is-rounded"><ion-icon name="arrow-back-outline"></ion-icon></a>
            <button type="submit" class="button is-success is-rounded">Save</button>
        </div>

    </form>
</div>

<script>
    document.title = "Create Power Unit | Test Skill Puninar Logistik";
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\testskill_puninar\resources\views/power_unit/create.blade.php ENDPATH**/ ?>