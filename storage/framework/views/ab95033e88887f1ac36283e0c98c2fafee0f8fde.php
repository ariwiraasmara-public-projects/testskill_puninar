
<?php $__env->startSection('content'); ?>
<div class="p-30 text-is-black">

    <div class="columns is-mobile">
        <div class="column is-4">
            <a href="/powerunit">
                <div class="box center">
                    <span class="title">Power Unit</span>
                </div>
            </a>
        </div>

        <div class="column is-4">
            <a href="/lookup/mh/">
                <div class="box center">
                    <span class="title">Lookup MH</span>
                </div>
            </a>
        </div>

        <div class="column is-4">
            <a href="/lookup/md/">
                <div class="box center">
                    <span class="title">Lookup MD</span>
                </div>
            </a>
        </div>
    </div>

    <div class="m-t-30 center">
        <a href="/maps">
            <div class="box center">
                <span class="title">Maps</span>
            </div>
        </a>
    </div>

    <div class="m-t-30 center">
        <h1 class="title">Deret Angka :</h1>
    </div>

    <div class="m-t-10 columns is-mobile">
        <div class="column is-3">
            <a href="/deretangka/7a/">
                <div class="box center">
                    <span class="title">7A</span>
                </div>
            </a>
        </div>

        <div class="column is-3">
            <a href="/deretangka/7b/">
                <div class="box center">
                    <span class="title">7B</span>
                </div>
            </a>
        </div>


        <div class="column is-3">
            <a href="/deretangka/7c/">
                <div class="box center">
                    <span class="title">7C</span>
                </div>
            </a>
        </div>

        <div class="column is-3">
            <a href="/deretangka/7d/">
                <div class="box center">
                    <span class="title">7D</span>
                </div>
            </a>
        </div>
    </div>
</div>

<script>
    document.title = "Dahsboard | Test Skill Puninar Logistik";
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\testskill_puninar\resources\views/home.blade.php ENDPATH**/ ?>