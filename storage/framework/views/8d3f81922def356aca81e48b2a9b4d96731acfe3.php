
<?php $__env->startSection('content'); ?>
<div class="p-30 text-is-black">
    <h1 class="title center text-is-black">Create Lookup MD</h1>

    <?php if(Session::has('msg')): ?>
        <div id="notif" class="notification is-danger m-t-10 m-b-10">
            <button id="close-notif" class="delete"></button>
            <?php echo e(Session::get('msg')); ?>

        </div>
    <?php endif; ?>

    <form action="<?php echo e(route('md_save')); ?>" method="post">
        <?php echo e(csrf_field()); ?>

        <?php echo e(method_field('POST')); ?>

        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <div class="columns">
            <div class="column is-6">
                <div class="control">
                    <div class="select is-fullwidth">
                        <select name="lookup_id">
                            <option value="" disabled selected>-- Choose Lookup MH --</option>
                            <?php $__currentLoopData = $lookupid; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lid): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($lid['lookup_id']); ?>"><?php echo e($lid['lookup_code']); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="column is-6">
                <div class="control">
                    <input class="input" type="text" name="lines_code" placeholder="Lookup Lines Code.." />
                </div>
            </div>
        </div>

        <div class="control m-t-10">
            <textarea class="textarea" name="description" placeholder="Description.."></textarea>
        </div>

        <div class="control m-t-10">
            <div class="columns">
                <div class="column is-6">
                    <label>Effective From: </label><br/>
                    <input type="date" class="input" name="effective_from" />
                </div>

                <div class="column is-6">
                    <label>Effective To: </label><br/>
                    <input type="date" class="input" name="effective_to" />
                </div>
            </div>
        </div>

        <button type="submit" class="button is-fullwidth is-link is-rounded center m-t-30">Save</button>
    </form>

</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\testskill_puninar\resources\views/lookup/md/create.blade.php ENDPATH**/ ?>