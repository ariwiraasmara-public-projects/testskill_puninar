
<?php $__env->startSection('content'); ?>
<div class="p-30">
    

    <div class="buttons is-centered has-addons is-fullwidth">
        <a href="/lookup/mh/" class="button is-link is-rounded"><ion-icon name="arrow-back-outline"></ion-icon></a>
        <p class="m-l-10 control"> 
            <h1 class="m-t-10 title text-is-black">Master Lookup MH Detail</h1>
        </p>
    </div>

    <table class="table is-fullwidth is-mobile">
        <thead>
            <tr>
                <th class="center">Lookup Lines ID</th>
                <th class="center">Lookup ID</th>
                <th>Lookup Lines Code</th>
                <th>Description</th>
                <th>Effective From</th>
                <th>Effective To</th>
            </tr>
        </thead>

        <tbody>
            <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $md): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td class="right"><?php echo e($md['lookup_lines_id']); ?></td>
                    <td class="right"><?php echo e($md['lookup_code']); ?></td>
                    <td><?php echo e($md['lookup_lines_code']); ?></td>
                    <td><?php echo e($md['description']); ?></td>
                    <td><?php echo e($md['effective_from']); ?></td>
                    <td><?php echo e($md['effective_to']); ?></td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
</div>

<script>

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\testskill_puninar\resources\views/lookup/mh/detail.blade.php ENDPATH**/ ?>