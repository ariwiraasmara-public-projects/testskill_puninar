
<?php $__env->startSection('content'); ?>
<div class="p-30">
    <h1 class="title center txt-black">Maps</h1>

    <div class="columns is-mobile">
        <div class="column is-6">
            <iframe src="https://maps.google.com/maps?q=Tangesir%20Dates%20Products&amp;t=&amp;z=13&amp;ie=UTF8&amp;iwloc=&amp;output=embed" 
                width=100% height=500 allowfullscreen></iframe>
        </div>

        <div class="column is-6">
            <div class="select is-fullwidth">
                <select name="location" id="location">
                    <option value="" disabled selected>-- Choose Location --</option>
                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $loc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($loc['ID_Location']); ?>"><?php echo e($loc['Location_Name']); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
            </div>
        </div>
    </div>

    
</div>

<script>
    document.title = "Maps | Skill Test Puninar Logistik"
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\testskill_puninar\resources\views/maps.blade.php ENDPATH**/ ?>