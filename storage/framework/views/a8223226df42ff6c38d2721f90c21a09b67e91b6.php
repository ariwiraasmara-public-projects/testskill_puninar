
<?php $__env->startSection('content'); ?>
<div class="p-30">
    <h1 class="title center txt-black">Create Lookup MH</h1>

    <?php if(Session::has('msg')): ?>
        <div id="notif" class="notification is-danger m-t-10 m-b-10">
            <button id="close-notif" class="delete"></button>
            <?php echo e(Session::get('msg')); ?>

        </div>
    <?php endif; ?>

    <form action="<?php echo e(route('mh_save')); ?>" method="post">
        <?php echo e(csrf_field()); ?>

        <?php echo e(method_field('POST')); ?>

        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <div class="control">
            <input class="input" type="text" name="lookup_code" placeholder="Lookup Code.." />
        </div>

        <div class="control m-t-10">
            <textarea class="textarea" name="description" placeholder="Description.."></textarea>
        </div>

        <button type="submit" class="button is-fullwidth is-link is-rounded center m-t-30">Save</button>
    </form>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\testskill_puninar\resources\views/lookup/mh/create.blade.php ENDPATH**/ ?>