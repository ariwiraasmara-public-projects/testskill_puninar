
<?php $__env->startSection('content'); ?>
<div class="p-30">
    <h1 class="title center txt-black">Master Lookup MH</h1>

    <div class="m-t-30 m-b-30 center">
        <a href="<?php echo e(route('mh_create')); ?>" class="button is-link is-rounded">Add</a>
        <?php if(Session::has('msg')): ?>
            <div id="notif" class="notification is-success m-t-10 m-b-10">
                <button id="close-notif" class="delete"></button>
                <?php echo e(Session::get('msg')); ?>

            </div>
        <?php endif; ?>
    </div>

    <table class="table is-fullwidth is-mobile">
        <thead>
            <tr>
                <th class="center">Lookup ID</th>
                <th class="">Lookup Code</th>
                <th class="">Description</th>
                <th colspan="2"></th>
            </tr>
        </thead>

        <tbody>
            <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mh): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td class="right"><?php echo e($mh['lookup_id']); ?></td>
                    <td><a href="<?php echo e('/lookup/mh/detail/'.$mh['lookup_id']); ?>"><?php echo e($mh['lookup_code']); ?></a></td>
                    <td><?php echo e($mh['description']); ?></td>
                    <td><a href="/lookup/mh/edit/<?php echo e($mh['lookup_id']); ?>" class="button is-link">Edit</a></td>
                    <td><button class="button is-danger" id="<?php echo e('deleteid'.$mh['lookup_id']); ?>" value="<?php echo e($mh['lookup_id']); ?>" onClick="toDelete(<?php echo e($mh['lookup_id']); ?>)" >Delete</button></td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
</div>

<script>
    function toDelete(idt) {
        let token   = $("meta[name='csrf-token']").attr("content");
        let id      = $('#deleteid' + idt).val();

        $.ajax({
            url: '/lookup/mh/delete/' + id,
            type : 'POST',
            data: {
                "_token": token,
			    id: id
			},  
			success: function(data) {
                alert('Data Berhasil Dihapus! Silahkan reload!');
			},
			error : function(error) {
			    alert('Telah terjadi error!\nTidak bisa delete data!');
                console.log(error);
			    return false;
			}
		});
    }

    $('#close-notif').click(function(){
        $('#notif').addClass('is-hidden');
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\testskill_puninar\resources\views/lookup/mh/list.blade.php ENDPATH**/ ?>