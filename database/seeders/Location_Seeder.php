<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Location_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('location')->insert([
            "ID_Location" => 1,
	        "Location_Name" => "POOL CILEGON",	
            "City" => "CILEGON",	
            "Province" => "BANTEN",
            "Latitude" => -6.002534,	
            "Longitude" => 106.011124, 
            "Insert_User" => 1,
             "Insert_Date" => '20/12/2018',
            "Update_User" => 1,
            "Update_Date" => '21/12/2018',
        ]);
            
        DB::table('location')->insert([
            "ID_Location" => 2,
	        "Location_Name" => "SURABAYA POOL",	
            "City" => "PANDAAN",	
            "Province" => "JAWA TIMUR",	
            "Latitude" => -7.650300,	
            "Longitude" => 112.705700,
            "Insert_User" => 1,
            "Insert_Date" => '20/12/2018',
            "Update_User" => 1,
            "Update_Date" => '21/12/2018',
        ]);

        DB::table('location')->insert([
            "ID_Location" => 3,
	        "Location_Name" => "POOL CAKUNG",	
            "City" => "CAKUNG",	
            "Province" => "DKI JAKARTA",	
            "Latitude" => -6.172035,	
            "Longitude" => 106.942108,
            "Insert_User" => 1,
            "Insert_Date" => '20/12/2018',
            "Update_User" => 1,
            "Update_Date" => '21/12/2018',
        ]);

        DB::table('location')->insert([
            "ID_Location" => 4,
	        "Location_Name" => "POOL NAGRAK",	
            "City" => "NAGRAK",	
            "Province" => "DKI JAKARTA",	
            "Latitude" => -6.116907,	
            "Longitude" => 106.942471,
            "Insert_User" => 1,
            "Insert_Date" => '20/12/2018',
            "Update_User" => 1,
            "Update_Date" => '21/12/2018',
        ]);

        DB::table('location')->insert([
            "ID_Location" => 5,
	        "Location_Name" => "POOL MEDAN",	
            "City" => "MEDAN",	
            "Province" => "SUMATRA UTARA",	
            "Latitude" => 3.590000,	
            "Longitude" => 98.678020,
            "Insert_User" => 1,
            "Insert_Date" => '20/12/2018',
            "Update_User" => 1,
            "Update_Date" => '21/12/2018',
        ]);
    }
}
