<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Corporation_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('corporation')->insert([
            'ID_Corporation' => 1,
            'Corporation_Name' => 'PT. Puninar Jaya',
            'Insert_User' => 1,
            'Insert_Date' => '20/12/2018',
            'Update_User' => 1,
            'Update_Date' => '21/12/2018',
        ]);
        
        DB::table('corporation')->insert([
            'ID_Corporation' => 2,
            'Corporation_Name' => 'PT. Puninar Infinite Jaya',
            'Insert_User' => 1,
            'Insert_Date' => '20/12/2018',
            'Update_User' => 1,
            'Update_Date' => '21/12/2018',
        ]);
    }
}
