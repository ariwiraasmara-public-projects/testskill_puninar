<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Power_Unit_Type_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('power_unit_type')->insert([
            "ID_Power_Unit_Type" => 1,	
            "Power_Unit_Type_XID" => "ENGKEL",	
            "Description" => "PRIME_MOVER ENGKEL NON KAROSERI"
        ]);

        DB::table('power_unit_type')->insert([
            "ID_Power_Unit_Type" => 2,	
            "Power_Unit_Type_XID" => "TRONTON",	
            "Description" => "PRIME_MOVER TRONTON NON KAROSERI"
        ]);

        DB::table('power_unit_type')->insert([
            "ID_Power_Unit_Type" => 3,	
            "Power_Unit_Type_XID" => "TOWING_CDD",	
            "Description" => "RIGID CDD JENIS KAROSERI TOWING"
        ]);

        DB::table('power_unit_type')->insert([
            "ID_Power_Unit_Type" => 4,	
            "Power_Unit_Type_XID" => "CAR-CARRIER_ENGKEL",	
            "Description" => "PRIME MOVER ENGKEL JENIS KAROSERI CAR CARRIER"
        ]);

        DB::table('power_unit_type')->insert([
            "ID_Power_Unit_Type" => 5,	
            "Power_Unit_Type_XID" => "MOTOR-CARRIER_ENGKEL",	
            "Description" => "PRIME MOVER ENGKEL JENIS KAROSERI MOTOR CARRIER"
        ]);

        DB::table('power_unit_type')->insert([
            "ID_Power_Unit_Type" => 6,	
            "Power_Unit_Type_XID" => "FLAT-DECK_TRONTON",	
            "Description" => "RIGID TRONTON JENIS KAROSERI FLAT DECK"
        ]);

        DB::table('power_unit_type')->insert([
            "ID_Power_Unit_Type" => 7,	
            "Power_Unit_Type_XID" => "MIXER_TRONTON",	
            "Description" => "RIGID TRONTON JENIS KAROSERI MIXER"
        ]);
    }
}
