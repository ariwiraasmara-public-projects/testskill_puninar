<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGnMhLookupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gn_mh_lookup', function (Blueprint $table) {
            $table->id('lookup_id');
            $table->string('lookup_code');
            $table->text('description')->nullable();
            $table->integer('insert_user')->nullable();
            $table->date('insert_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gn_mh_lookup');
    }
}
