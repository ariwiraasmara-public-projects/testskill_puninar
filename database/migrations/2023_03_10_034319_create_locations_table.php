<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location', function (Blueprint $table) {
            $table->id('ID_Location');
            $table->string('Location_Name');
            $table->string('City');
            $table->string('Province');
            $table->decimal('Latitude')->nullable();
            $table->decimal('Longitude')->nullable();
            $table->integer('Insert_User')->nullable();
            $table->date('Insert_Date')->nullable();
            $table->integer('Update_User')->nullable();
            $table->date('Update_Date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location');
    }
}
