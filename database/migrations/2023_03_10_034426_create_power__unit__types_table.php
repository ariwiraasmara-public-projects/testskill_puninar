<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePowerUnitTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('power_unit_type', function (Blueprint $table) {
            $table->id('ID_Power_Unit_Type');
            $table->string('Power_Unit_Type_XID');
            $table->text('Description');
            $table->integer('Insert_User')->nullable();
            $table->date('Insert_Date')->nullable();
            $table->integer('Update_User')->nullable();
            $table->date('Update_Date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('power_unit_type');
    }
}
