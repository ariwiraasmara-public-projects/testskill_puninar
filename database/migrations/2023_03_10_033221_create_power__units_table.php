<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePowerUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('power_unit', function (Blueprint $table) {
            $table->id('ID_Power_Unit');
            $table->string('Power_Unit_Num');
            $table->text('Description');
            $table->integer('ID_Corporation');
            $table->integer('ID_Location');
            $table->integer('ID_Power_Unit_Type');
            $table->char('Is_Active', 1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('power_unit');
    }
}
