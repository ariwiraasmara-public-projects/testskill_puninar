<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class gn_md_lookup extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'gn_md_lookup';
    protected $primaryKey = 'lookup_lines_id';
    protected $fillable = ["lookup_lines_id",
                            "lookup_code",
                            "lookup_lines_code",
                            "description",
                            "effective_from",
                            "effective_to",
                            "insert_user",
                            "insert_date"];

    public $timestamps = false;
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';
}
