<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Power_Unit extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'power_unit';
    protected $primaryKey = 'ID_Power_Unit';
    protected $fillable = ["ID_Power_Unit",
                            "Power_Unit_Num",
                            "Description",
                            "ID_Corporation",
                            "ID_Location",
                            "ID_Power_Unit_Type",
                            "Is_Active"];

    public $timestamps = false;
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';
}
