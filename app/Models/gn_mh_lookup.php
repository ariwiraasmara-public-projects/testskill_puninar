<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class gn_mh_lookup extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'gn_mh_lookup';
    protected $primaryKey = 'lookup_id';
    protected $fillable = ["lookup_id",
                            "lookup_code",
                            "description",
                            "insert_user",
                            "insert_date",];

    public $timestamps = false;
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';
}
