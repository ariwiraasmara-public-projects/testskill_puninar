<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'location';
    protected $primaryKey = 'ID_Location';
    protected $fillable = ["ID_Location",
                            "Location_Name",
                            "City",
                            "Province",
                            "Latitude",
                            "Longitude",
                            "Insert_User",
                            "Insert_Date",
                            "Update_User",
                            "Update_Date"];

    public $timestamps = false;
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';
}
