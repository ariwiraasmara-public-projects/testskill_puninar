<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Corporation extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'corporation';
    protected $primaryKey = 'ID_Corporation';
    protected $fillable = ["ID_Corporation",
                            "Corporation_Name",
                            "Insert_User",
                            "Insert_Date",
                            "Update_User",
                            "Update_Date",];

    public $timestamps = false;
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';
}
