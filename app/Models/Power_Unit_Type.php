<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Power_Unit_Type extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'power_unit_type';
    protected $primaryKey = 'ID_Power_Unit_Type';
    protected $fillable = ["ID_Power_Unit_Type",
                            "Power_Unit_Type_XID",
                            "Description",
                            "Insert_User",
                            "Insert_Date",
                            "Update_User",
                            "Update_Date"];

    public $timestamps = false;
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';
}
