<?php

namespace App\Http\Controllers;

use App\Models\gn_mh_lookup;
use App\Models\gn_md_lookup;
use Illuminate\Http\Request;
use Redirect;

class GnMdLookupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = gn_md_lookup::orderBy('lookup_lines_id', 'asc')->get();
        return view('lookup.md.list', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data = gn_mh_lookup::orderBy('lookup_code', 'asc')->get();
        return view('lookup.md.create', ['lookupid'=>$data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $getdata = gn_md_lookup::select('lookup_lines_id')
                             ->orderby('lookup_lines_id', 'desc')
                             ->get();

        if( $getdata->isEmpty() ) $id = 1;
        if( !$getdata->isEmpty() ) $id = (int)$getdata[0]["lookup_lines_id"] + 1;
        
        $res = gn_md_lookup::create([
            "lookup_lines_id"   => $id,
            "lookup_code"       => $request->lookup_id,
            "lookup_lines_code" => $request->lines_code,
            "description"       => $request->description,
            "effective_from"    => $request->effective_from,
            "effective_to"      => $request->effective_to,
            "insert_user"       => 1,
            "insert_date"       => date('Y-m-d')
        ]);

        if($res) return Redirect::to('lookup/md')->with(['msg' => 'Create LookUp MD Success!']);
        else return Redirect::to('lookup/md/create')->with(['msg' => 'Create LookUp MD Fail!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\gn_md_lookup  $gn_md_lookup
     * @return \Illuminate\Http\Response
     */
    public function show(gn_md_lookup $gn_md_lookup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\gn_md_lookup  $gn_md_lookup
     * @return \Illuminate\Http\Response
     */
    public function edit(gn_md_lookup $gn_md_lookup, $id)
    {
        //
        $data = gn_md_lookup::where('lookup_lines_id', '=', $id)->get();
        $datamh = gn_mh_lookup::orderBy('lookup_code', 'asc')->get();
        return view('lookup.md.edit', ['data'=>$data, 'lookupid'=>$datamh]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\gn_md_lookup  $gn_md_lookup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, gn_md_lookup $gn_md_lookup)
    {
        //
        $res = gn_md_lookup::where('lookup_lines_id', '=', $request->id)->update([
            "lookup_code"       => $request->lookup_id,
            "lookup_lines_code" => $request->lines_code,
            "description"       => $request->description,
            "effective_from"    => $request->effective_from,
            "effective_to"      => $request->effective_to,
        ]);

        if($res) return Redirect::to('lookup/md')->with(['msg' => 'Update LookUp MD Success!']);
        else return Redirect::to('lookup/md/edit/'.$request->id)->with(['msg' => 'Update LookUp MD Fail!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\gn_md_lookup  $gn_md_lookup
     * @return \Illuminate\Http\Response
     */
    public function destroy(gn_md_lookup $gn_md_lookup, $id)
    {
        //
        gn_md_lookup::where('lookup_lines_id', '=', $id)->delete();
    }
}
