<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DeretAngka extends Controller {
    //
    public function deretangka7a() {
        return view('deret_angka.7a');
    }

    public function deretangka7b() {
        return view('deret_angka.7b');
    }

    public function deretangka7c() {
        return view('deret_angka.7c');
    }

    public function deretangka7d() {
        return view('deret_angka.7d');
    }
}
