<?php

namespace App\Http\Controllers;

use App\Models\Power_Unit_Type;
use Illuminate\Http\Request;

class PowerUnitTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Power_Unit_Type  $power_Unit_Type
     * @return \Illuminate\Http\Response
     */
    public function show(Power_Unit_Type $power_Unit_Type)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Power_Unit_Type  $power_Unit_Type
     * @return \Illuminate\Http\Response
     */
    public function edit(Power_Unit_Type $power_Unit_Type)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Power_Unit_Type  $power_Unit_Type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Power_Unit_Type $power_Unit_Type)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Power_Unit_Type  $power_Unit_Type
     * @return \Illuminate\Http\Response
     */
    public function destroy(Power_Unit_Type $power_Unit_Type)
    {
        //
    }
}
