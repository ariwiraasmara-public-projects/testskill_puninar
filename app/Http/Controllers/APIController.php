<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Power_Unit;

class APIController extends Controller
{
    //
    public function login(Request $request) {
        $validatedData = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if(!auth()->attempt($validatedData)) {
            return response(['message' => 'Invalid Credentials']);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;
        return response(['user' => auth()->user(), 'access_token' => $accessToken]);
    }

    public function powerunit_alldata() {
        $data = Power_Unit::select('power_unit.ID_Power_Unit', 'power_unit.Power_Unit_Num', 'power_unit.Description', 
                                   'location.ID_Location', 'location.Location_Name',
                                   'corporation.ID_Corporation', 'corporation.Corporation_Name',
                                   'power_unit_type.ID_Power_Unit_Type', 'power_unit_type.Power_Unit_Type_XID')
                          ->join('location', 'location.ID_Location', '=', 'power_unit.ID_Location')
                          ->join('corporation', 'corporation.ID_Corporation', '=', 'power_unit.ID_Corporation')
                          ->join('power_unit_type', 'power_unit_type.ID_Power_Unit_Type', '=', 'power_unit.ID_Power_Unit_Type')
                          ->orderBy('power_unit.ID_Power_Unit', 'asc')
                          ->get();
        return response()->json(['msg' => 'Power Unit Data', 'success' => 1, 'data'=>$data], 200);
    }
}
