<?php

namespace App\Http\Controllers;

use App\Models\Power_Unit;
use App\Models\Power_Unit_Type;
use App\Models\Location;
use App\Models\Corporation;
use Illuminate\Http\Request;
use Redirect;

class PowerUnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Power_Unit::select('power_unit.ID_Power_Unit', 'power_unit.Power_Unit_Num', 
                                   'power_unit.Description', 'power_unit.Is_Active',
                                   'location.ID_Location', 'location.Location_Name',
                                   'corporation.ID_Corporation', 'corporation.Corporation_Name',
                                   'power_unit_type.ID_Power_Unit_Type', 'power_unit_type.Power_Unit_Type_XID')
                          ->join('location', 'location.ID_Location', '=', 'power_unit.ID_Location')
                          ->join('corporation', 'corporation.ID_Corporation', '=', 'power_unit.ID_Corporation')
                          ->join('power_unit_type', 'power_unit_type.ID_Power_Unit_Type', '=', 'power_unit.ID_Power_Unit_Type')
                          ->orderBy('power_unit.ID_Power_Unit', 'asc')
                          ->get();
        return view('power_unit.tablelist', ['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $power_unit_type = Power_Unit_Type::select('ID_Power_Unit_Type', 'Power_Unit_Type_XID')
                                            ->orderBy('Power_Unit_Type_XID', 'asc')
                                            ->get();

        $location = Location::select('ID_Location', 'Location_Name')
                              ->orderBy('Location_Name', 'asc')
                              ->get();

        $corporation = Corporation::select('ID_Corporation', 'Corporation_Name')
                                    ->orderBy('Corporation_Name', 'asc')
                                    ->get();

        return view('power_unit.create', 
                    ['power_unit_type'=> $power_unit_type,
                    'location' => $location,
                    'corporation' => $corporation                    
                    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $getdata = Power_Unit::select('ID_Power_Unit')
                             ->orderby('ID_Power_Unit', 'desc')
                             ->get();

        if( $getdata->isEmpty() ) $id = 1;
        if( !$getdata->isEmpty() ) $id = (int)$getdata[0]["ID_Power_Unit"] + 1;
        
        $res = Power_Unit::create(
            [
                "ID_Power_Unit"         => $id,
                "Power_Unit_Num"        => $request->powerunit,
                "Description"           => $request->description,
                "ID_Corporation"        => $request->corporation,
                "ID_Location"           => $request->location,
                "ID_Power_Unit_Type"    => $request->type,
                "Is_Active"             => $request->isactive,
                "Insert_User"           => 1,
                "Insert_Date"           => date('Y-m-d'),
            ]
        );
        
        if($res) return Redirect::to('powerunit')->with(['msg' => 'Create Power Unit Success!']);
        else return Redirect::to('powerunit/create')->with(['msg' => 'Create Power Unit Fail!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Power_Unit  $power_Unit
     * @return \Illuminate\Http\Response
     */
    public function show(Power_Unit $power_Unit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Power_Unit  $power_Unit
     * @return \Illuminate\Http\Response
     */
    public function edit(Power_Unit $power_Unit, $id)
    {
        //
        $data = Power_Unit::where('ID_Power_Unit', '=', $id)->get();

        $power_unit_type = Power_Unit_Type::select('ID_Power_Unit_Type', 'Power_Unit_Type_XID')
                                            ->orderBy('Power_Unit_Type_XID', 'asc')
                                            ->get();

        $location = Location::select('ID_Location', 'Location_Name')
                              ->orderBy('Location_Name', 'asc')
                              ->get();

        $corporation = Corporation::select('ID_Corporation', 'Corporation_Name')
                                    ->orderBy('Corporation_Name', 'asc')
                                    ->get();

        return view('power_unit.edit', 
                    ['data' => $data,
                     'power_unit_type'=> $power_unit_type,
                     'location' => $location,
                     'corporation' => $corporation                    
                    ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Power_Unit  $power_Unit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Power_Unit $power_Unit)
    {
        //
        $res = Power_Unit::where('ID_Power_Unit', '=', $request->id)->update(
            [
                "Power_Unit_Num"        => $request->powerunit,
                "Description"           => $request->description,
                "ID_Corporation"        => $request->corporation,
                "ID_Location"           => $request->location,
                "ID_Power_Unit_Type"    => $request->type,
                "Is_Active"             => $request->isactive
            ]
        );

        if($res) return Redirect::to('powerunit')->with(['msg' => 'Update Power Unit Success!']);
        else return Redirect::to('powerunit/edit/'.$request->id)->with(['msg' => 'Update Power Unit Fail!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Power_Unit  $power_Unit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Power_Unit $power_Unit, $id)
    {
        //
        Power_Unit::where('ID_Power_Unit', '=', $id)->delete();
    }
}
