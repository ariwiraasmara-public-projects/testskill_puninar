<?php

namespace App\Http\Controllers;

use App\Models\gn_mh_lookup;
use App\Models\gn_md_lookup;
use Illuminate\Http\Request;
use Redirect;

class GnMhLookupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = gn_mh_lookup::orderBy('lookup_id', 'asc')->get();
        return view('lookup.mh.list', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('lookup.mh.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $getdata = gn_mh_lookup::select('lookup_id')
                             ->orderby('lookup_id', 'desc')
                             ->get();

        if( $getdata->isEmpty() ) $id = 1;
        if( !$getdata->isEmpty() ) $id = (int)$getdata[0]["lookup_id"] + 1;
        
        $res = gn_mh_lookup::create([
            "lookup_id"     => $id,
            "lookup_code"   => $request->lookup_code,
            "description"   => $request->description,
            "insert_user"   => 1,
            "insert_date"   => date('Y-m-d')
        ]);

        if($res) return Redirect::to('lookup/mh')->with(['msg' => 'Create LookUp MH Success!']);
        else return Redirect::to('lookup/mh/create')->with(['msg' => 'Create LookUp MH Fail!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\gn_mh_lookup  $gn_mh_lookup
     * @return \Illuminate\Http\Response
     */
    public function show(gn_mh_lookup $gn_mh_lookup, $id)
    {
        //
        $data = gn_md_lookup::join('gn_mh_lookup', 'gn_mh_lookup.lookup_id', '=', 'gn_md_lookup.lookup_code')
                            ->where('gn_md_lookup.lookup_code', '=', $id)
                            ->orderBy('gn_md_lookup.lookup_lines_code', 'asc')
                            ->get();
        
        return view('lookup/mh/detail', ['data'=>$data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\gn_mh_lookup  $gn_mh_lookup
     * @return \Illuminate\Http\Response
     */
    public function edit(gn_mh_lookup $gn_mh_lookup, $id)
    {
        //
        $data = gn_mh_lookup::where('lookup_id', '=', $id)->get();
        return view('lookup.mh.edit', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\gn_mh_lookup  $gn_mh_lookup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, gn_mh_lookup $gn_mh_lookup)
    {
        //
        $res = gn_mh_lookup::where('lookup_id', '=', $request->id)->update([
            "lookup_code"   => $request->lookup_code,
            "description"   => $request->description,
        ]);

        if($res) return Redirect::to('lookup/mh')->with(['msg' => 'Update LookUp MH Success!']);
        else return Redirect::to('lookup/mh/edit/'.$request->id)->with(['msg' => 'Update LookUp MH Fail!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\gn_mh_lookup  $gn_mh_lookup
     * @return \Illuminate\Http\Response
     */
    public function destroy(gn_mh_lookup $gn_mh_lookup, $id)
    {
        //
        gn_mh_lookup::where('lookup_id', '=', $id)->delete();
    }
}
